package sample;

import java.awt.Color;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

/**
 * Login Controller.
 */
public class LoginController extends AnchorPane implements Initializable {

    @FXML
    TextField email;
    @FXML
    PasswordField password;
    @FXML
    Button accedi;
    @FXML
    Button registrazione;
    @FXML
    Hyperlink dimenticata;
    @FXML
    Label errore;

    private MeetFlyAPP application;


    public void setApp(MeetFlyAPP application){
        this.application = application;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errore.setText("");
        email.setPromptText("email");
        password.setPromptText("password");

    }


    public void processLogin(ActionEvent event) {
        if (application == null){
            // We are running in isolated FXML, possibly in Scene Builder.
            // NO-OP.
            errore.setText("Hello " + email.getText());
        } else {
            //if (!application.userLogging(email.getText(), password.getText())){
                errore.setText("Username/Password is incorrect");
            //}
        }
    }
}
